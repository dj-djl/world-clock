const clocks = document.querySelectorAll('div[data-tz]');
for (let divClock of clocks) {
    divClock.innerHTML = `<span class="date"></span><span class="time"></span><input type="time"></input>`;
    const flag = divClock.getAttribute('data-flag');
    if (flag) {
        divClock.style.backgroundImage = `url(${flag})`;
        console.log('setting flag', flag);
    }
}
function updateTime () {
    const currentTime = moment();
    for (let divClock of clocks) {
        let localTime = moment().tz(divClock.getAttribute('data-tz'));
        divClock.querySelector('.date').innerText =localTime.format('ddd');
        divClock.querySelector('.time').innerText =localTime.format('HH:mm:ss');
        // divClock.innerHTML = `<span class="date">${localTime.format('ddd')}</span><span class="time">${localTime.format('HH:mm:ss')}</span>`;
        
    }
    let bottom = clocks[clocks.length-1].offsetTop + clocks[clocks.length-1].offsetHeight;
    let availableHeight = document.getElementById('Container').offsetHeight;
    document.documentElement.style.fontSize = Math.floor(10*(parseFloat(document.documentElement.style.fontSize || '10') * (availableHeight / (bottom))))/10 + 'px';
}

setInterval(updateTime, 200);

for (let input of document.querySelectorAll('div[data-tz]>input') ) {
    input.onchange = function() {
        const localZone = input.parentElement.getAttribute('data-tz');
        let localTime = moment.tz(moment().tz(localZone).format('YYYY-MM-DD') + ' ' + input.value, localZone);
        console.log(localTime);
        for (let input2 of document.querySelectorAll('div[data-tz]>input') ) {
            if (input2!==input) {
                const otherZone = input2.parentElement.getAttribute('data-tz');
                console.log(localTime.clone().tz(otherZone).format('HH:mm'));
                input2.value =localTime.clone().tz(otherZone).format('HH:mm');
            }
        }
    }
}