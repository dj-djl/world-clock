
#How to use

```
npm install 
npm start
```

Open browser at localhost:1200

Once the page has loaded it is self autonomous, so you can close the node app if you desire.

#Fonts
I can't include my preferred clock font due to distribution restrictions.
Just search for free clock fonts, and save as "clock.ttf". It should be picked up automatically
