import http from 'http';
import fs from 'fs';

const pages = {
    '/': { fileName: 'worldclock.html', contentType: 'text/html'},
    '/index.html': { fileName: 'worldclock.html', contentType: 'text/html'},
    '/worldclock.html': { fileName: 'worldclock.html', contentType: 'text/html'},
    '/clocks.mjs': {fileName: 'clocks.mjs', contentType: 'application/javascript'},
    '/moment-with-locales.js': {fileName: 'node_modules/moment/min/moment-with-locales.min.js', contentType: 'application/javascript'},
    '/moment-timezone-with-data.js': {fileName: 'node_modules/moment-timezone/builds/moment-timezone-with-data.min.js', contentType: 'application/javascript'},
    '/styles.css': {fileName: 'styles.css', contentType: 'text/css'},    
    '/clock.ttf': {fileName: 'clock.ttf', contentType: 'application/font'},    
    '/Spain.png': {fileName: 'Spain.png', contentType: 'image/png'},    
    '/UK.png': {fileName: 'UK.png', contentType: 'image/png'},    
    '/NZ.png': {fileName: 'NZ.png', contentType: 'image/png'},    
    '/France.png': {fileName: 'France.png', contentType: 'image/png'},    
    '/Morocco.png': {fileName: 'Morocco.png', contentType: 'image/png'},    
}

http.createServer((req, resp)=> {
    console.log(req.url);
    
    if (req.url in pages) {
        console.log(pages[req.url]);
        resp.writeHead(200, {"content-type": pages[req.url].contentType});
        fs.createReadStream(pages[req.url].fileName).pipe(resp);
    } else {
        resp.writeHead(404, 'text/plain');
        resp.end('404 innit');
    }
}).listen(1200);

